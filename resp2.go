package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"
)

func main() {
	bw := bufio.NewWriter(os.Stdout)
	if len(os.Args) == 1 {
		noErr(render(os.Stdin, bw))
	} else {
		for i := 2; i < len(os.Args); i++ {
			f, err := os.Open(os.Args[i])
			noErr(err)
			if err := render(f, bw); err != nil {
				log.Fatalf("%s: %v", f.Name(), err)
			}
			noErr(f.Close())
		}
	}

	noErr(bw.Flush())
}

func noErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

var (
	crlf         = []byte("\r\n")
	array        = []byte("*")
	bulkString   = []byte("$")
	startCommand = regexp.MustCompile(`\A\*[0-9]+\z`)
)

func render(r io.Reader, bw *bufio.Writer) (err error) {
	defer func() {
		if errors.Is(err, io.EOF) {
			err = nil
		}
	}()

	br := bufio.NewReader(r)
	line, err := readLine(br, nil)
	if err != nil {
		return fmt.Errorf("read first line: %w", err)
	}
	for !startCommand.Match(line) {
		line, err = readLine(br, line)
		if err != nil {
			return fmt.Errorf("search for start of array: %w", err)
		}
	}

	for {
		// Either tcpdump or tcpflow can leave blocks of zero bytes in the
		// capture data. We want to ignore those.
		line = bytes.TrimLeft(line, "\x00")

		n, err := strconv.Atoi(string(line[len(array):]))
		if err != nil {
			return fmt.Errorf("parse array length: %w", err)
		}

		// Parse the n bulk strings that make up the current command.
		for i := 0; i < n; i++ {
			line, err = readLine(br, line)
			if err != nil {
				return err
			}
			if !bytes.HasPrefix(line, bulkString) {
				return errors.New("expected bulk string header")
			}
			m, err := strconv.Atoi(string(line[len(bulkString):]))
			if err != nil {
				return fmt.Errorf("parse bulk string length: %w", err)
			}
			// The current bulk string is m bytes long. We escape \r and \n because
			// we want to generate grep-friendly output.
			for j := 0; j < m; j++ {
				b, err := br.ReadByte()
				if err != nil {
					return err
				}
				if b == '\n' || b == '\r' {
					b = ' '
				}
				if err := bw.WriteByte(b); err != nil {
					return err
				}
			}
			line, err = readLine(br, line)
			if len(line) > 0 {
				return errors.New("expected empty line after string")
			}
			sep := " "
			if i == n-1 {
				sep = "\n"
			}
			if _, err := bw.WriteString(sep); err != nil {
				return err
			}
		}
		line, err = readLine(br, line)
		if err != nil {
			return err
		}
	}
}

func readLine(r *bufio.Reader, line []byte) ([]byte, error) {
	line = line[:0]
	for err := bufio.ErrBufferFull; err == bufio.ErrBufferFull; {
		var data []byte
		data, err = r.ReadSlice('\n')
		if err != bufio.ErrBufferFull && err != nil {
			return nil, err
		}
		line = append(line, data...)
	}
	if !bytes.HasSuffix(line, crlf) {
		return nil, errors.New("invalid line ending")
	}

	return line[:len(line)-len(crlf)], nil
}
